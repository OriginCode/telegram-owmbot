from pyowm import OWM
import json
from telegram import ParseMode
from modules.deghandler import __deghandler__

__all__ = ['owmweather']

with open('./config.json') as f:
    config = json.load(f)

# TODO: Seperate the owm parser core and the handler.
def owmweather(update, context):
    if len(context.args) == 0:
        update.message.reply_text(
            'Usage: /owmweather <City>[,Country ID] [Num of Reqs (Default: 2)]')
        return 1

    owm = OWM(API_key=config['OWM']['APPID'])
    city = ""
    for i in context.args:
        if not i.isdigit():
            city += i
            city += " "
    city = city[:len(city) - 1]

    if context.args[len(context.args) - 1].isdigit():
        lim = int(context.args[len(context.args) - 1])
        obs = owm.weather_at_places(
            city, searchtype='accurate', limit=lim + 1)
    else:
        obs = owm.weather_at_places(city, searchtype='accurate', limit=3)

    if len(obs) == 0:
        update.message.reply_text(
            '*Invalid City Name!*', parse_mode=ParseMode.MARKDOWN)
        return 1

    for i in range(len(obs)):
        w = obs[i].get_weather()
        l = obs[i].get_location()

        country = l.get_country()
        city = l.get_name()
        lon = l.get_lon()
        lat = l.get_lat()
        weather = w.get_status()
        temp = w.get_temperature(unit='celsius')['temp']
        humidity = w.get_humidity()
        wind_speed = w.get_wind()['speed']
        wind_deg = w.get_wind()['deg']

        update.message.reply_text(
            f'*Current weather in {city}, {country} (lon:{lon:.4} lat:{lat:.4})*\n'
            f'Weather: {weather}\n'
            f'Current Temperature: {temp} °C\n'
            f'Humidity: {humidity}%\n'
            f'Wind Speed: {wind_speed} m/s\n'
            f'Wind Degree: {__deghandler__(wind_deg)} {wind_deg}°',
            parse_mode=ParseMode.MARKDOWN)
